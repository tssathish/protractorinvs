﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using Protractor;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using System.Threading;

namespace ProtractorTest
{
    [TestClass]
    [DeploymentItem("chromedriver.exe")]
    public class UnitTest1
    {
        IWebDriver driver;
        NgWebDriver ngdriver;


        [TestInitialize]
        public void Setup()
        {
            //OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            //driver = new RemoteWebDriver(
            //  new Uri("https://www.625.yepdesk.com/sign-up"), capability
            //);
            //driver = new ChromeDriver();

            //var chromeOptions = new ChromeOptions();
            //chromeOptions.AddArgument("--ignore-certificate-errors");//
            //driver = new ChromeDriver(chromeOptions);

            //driver = new FirefoxDriver();
            //driver.Manage().Timeouts().SetScriptTimeout(TimeSpan.FromSeconds(10));
            
            OpenQA.Selenium.Remote.DesiredCapabilities capability = DesiredCapabilities.Chrome();
            capability.SetCapability("browserstack.user", "sathishsundar1");
            capability.SetCapability("browserstack.key", "YGm51SNDeDSgvyYvLdah");

            driver = new RemoteWebDriver(
              new Uri("http://hub.browserstack.com/wd/hub/"), capability
            );
            driver.Manage().Timeouts().SetScriptTimeout(TimeSpan.FromSeconds(10));
            ngdriver = new NgWebDriver(driver);
        }

        [TestMethod]
        public void TestMethod1()
        {
            ngdriver.Url = "https://www.625.yepdesk.com/sign-up"; // navigate to URL

            //browser.get('https://www.625.yepdesk.com/sign-up');
            //element(by.model('user.email')).sendKeys('sathishatAOT2@gmail.com');
            //element(by.model('user.firstName')).sendKeys('Sathish');
            //element(by.model('user.lastName')).sendKeys('TS');
            //element(by.model('user.password')).sendKeys('pa@ssw0rd');
            //element(by.id('btn-signup')).click();

            ngdriver.FindElement(NgBy.Model("user.email")).SendKeys("sathishatAOT2@gmail.com");
            ngdriver.FindElement(NgBy.Model("user.firstName")).SendKeys("Sathish");
            ngdriver.FindElement(NgBy.Model("user.lastName")).SendKeys("TS");
            ngdriver.FindElement(NgBy.Model("user.password")).SendKeys("pa@ssw0rd");
            ngdriver.FindElement(By.Id("btn-signup")).Click();

            //var latestResult = ngDriver.FindElement(NgBy.Binding("latest")).Text;
            //latestResult.Should().Be("3");
        }

        //redundant code, please delete it later
        [TestMethod]
        public void TestMethod2()
        {
            ngdriver.Url = "https://www.625.yepdesk.com/sign-up"; // navigate to URL

            //browser.get('https://www.625.yepdesk.com/sign-up');
            //element(by.model('user.email')).sendKeys('sathishatAOT2@gmail.com');
            //element(by.model('user.firstName')).sendKeys('Sathish');
            //element(by.model('user.lastName')).sendKeys('TS');
            //element(by.model('user.password')).sendKeys('pa@ssw0rd');
            //element(by.id('btn-signup')).click();

            ngdriver.FindElement(NgBy.Model("user.email")).SendKeys("sathishatAOT2@gmail.com");
            ngdriver.FindElement(NgBy.Model("user.firstName")).SendKeys("Sathish");
            ngdriver.FindElement(NgBy.Model("user.lastName")).SendKeys("TS");
            ngdriver.FindElement(NgBy.Model("user.password")).SendKeys("pa@ssw0rd2");
            ngdriver.FindElement(By.Id("btn-signup")).Click();

            //var latestResult = ngDriver.FindElement(NgBy.Binding("latest")).Text;
            //latestResult.Should().Be("3");
        }

        [TestMethod]
        public void ExecuteSignIn()
        {
            ngdriver.Url = "https://www.625.yepdesk.com/sign-in"; // navigate to URL

            ngdriver.FindElement(NgBy.Model("user.email")).SendKeys("sathishatAOT2@gmail.com");
            ngdriver.FindElement(NgBy.Model("user.password")).SendKeys("pa@ssw0rd");

            ngdriver.FindElement(By.CssSelector("button[ng-click='loginUser(user)']")).Click();
            //driver.FindElement(By.XPath("//button[text()='Login']")).Click();

            Thread.Sleep(9000);
            ngdriver.Quit();//
        }
    }
}
